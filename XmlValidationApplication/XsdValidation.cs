using System;
using System.Xml;
using System.Xml.Schema;

namespace XmlValidators
{
    public class XsdValidation
    {
        private bool _bSuccess = true;
        private string _xmlFile;
        private string _targetNamespace;
        private string _xsdFile;
        private XmlReader _reader;
        public void AddXmlFileAndNamespace(string filename, string targetNamespace)
        {
            _xmlFile = filename;
            _targetNamespace = targetNamespace;
        }
        
        public void AddXsdFile(string filename)
        {
            _xsdFile = filename;
        }
        public void ValidateXml()
        {
            var readerSettings = new XmlReaderSettings();
            readerSettings.Schemas.Add(_targetNamespace, _xsdFile);
            readerSettings.ValidationType = ValidationType.Schema;
            readerSettings.ValidationEventHandler += XmlValidationEventHandler;

            _reader = XmlReader.Create(_xmlFile, readerSettings);
            Console.WriteLine("\nValidating XML file " + _xmlFile + " using " + "XSD");

            while (_reader.Read()) { }
            
            Console.WriteLine("Validation using XSD finished. Validation {0}",
                (_bSuccess ? "successful" : "failed"));
        }

        private void XmlValidationEventHandler(object sender, ValidationEventArgs e)
        {
            switch (e.Severity)
            {
                case XmlSeverityType.Warning:
                    _bSuccess = false;
                    Console.Write("\r\n\tValidation error at line  " + ((IXmlLineInfo)_reader).LineNumber + " position " +
                                  ((IXmlLineInfo)_reader).LinePosition + ": " + e.Message);
                    break;
                case XmlSeverityType.Error:
                    _bSuccess = false;
                    Console.Write("\r\n\tValidation error at line  " + ((IXmlLineInfo)_reader).LineNumber + " position " +
                                  ((IXmlLineInfo)_reader).LinePosition + ": " + e.Message);
                    break;
            }
        }
    }
}