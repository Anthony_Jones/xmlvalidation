﻿using System;
using System.Xml;
using System.Xml.Schema;

namespace XmlValidators
{
    public class XdrAndDtdValidation
    {
        private string _xmlFile;

        private XmlTextReader _reader;
#pragma warning disable 618
        private XmlValidatingReader _validatingReader;
#pragma warning restore 618
        private bool _bSuccess = true;

        public void AddXmlFile(string filename)
        {
            _xmlFile = filename;
        }


        public void ValidateXml()
        {
#pragma warning disable 618
            Validate(_xmlFile, ValidationType.XDR);
#pragma warning restore 618
            Validate(_xmlFile, ValidationType.DTD);
        }

        private void Validate(string filename, ValidationType vt)
        {
            try
            {
                _reader = new XmlTextReader(filename);
#pragma warning disable 618
                _validatingReader = new XmlValidatingReader(_reader);
#pragma warning restore 618
                _validatingReader.ValidationType = vt;

                Console.WriteLine("\nValidating XML file " + filename + " using " + vt);
                _bSuccess = true;
                _validatingReader.ValidationEventHandler += XmlValidationEventHandler;

                while (_validatingReader.Read())
                {
                }

                Console.WriteLine("Validation using " + vt + " finished. Validation {0}",
                    (_bSuccess ? "successful" : "failed"));
            }

            finally
            {
                _validatingReader?.Close();
            }
        }

        private void XmlValidationEventHandler(object sender, ValidationEventArgs args)
        {
            _bSuccess = false;

            Console.Write("\r\n\tValidation error at line " + ((IXmlLineInfo)_reader).LineNumber + " position " +
                          ((IXmlLineInfo)_reader).LinePosition + ": " + args.Message);
        }
    }
}