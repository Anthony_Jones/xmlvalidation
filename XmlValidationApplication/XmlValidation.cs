using XmlValidators;

namespace XmlValidationApplication
{
    public static class XmlValidation
    {
        public static void Main()
        {
            var xdrAndDtdValidation = new XdrAndDtdValidation();
            xdrAndDtdValidation.AddXmlFile("DeviceWithDTDAndXDR.xml");
            xdrAndDtdValidation.ValidateXml();
            
            var xsdValidation = new XsdValidation();
            xsdValidation.AddXsdFile("DeviceXSD.xsd");
            xsdValidation.AddXmlFileAndNamespace("DeviceWithoutDTD.xml", "Device");
            xsdValidation.ValidateXml();
            
            xdrAndDtdValidation.AddXmlFile("DeviceInvalidDTDandXRD.xml");
            xdrAndDtdValidation.ValidateXml();

        }

    }
}